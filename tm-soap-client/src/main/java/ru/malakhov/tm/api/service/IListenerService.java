package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.listener.AbstractListener;

import java.util.Map;
import java.util.Set;

public interface IListenerService {

    @NotNull
    Map<String, AbstractListener> getCommands();

    @NotNull
    Set<String> getCommandsName();

    @NotNull
    Set<String> getCommandsArg();

}
