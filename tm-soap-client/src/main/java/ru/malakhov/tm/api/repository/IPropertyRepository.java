package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IPropertyRepository {

    @NotNull
    List<String> getCookieHeaders();

    void setCookieHeaders(@NotNull List<String> cookieHeaders);

}
