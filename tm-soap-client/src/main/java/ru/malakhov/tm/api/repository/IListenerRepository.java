package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.listener.AbstractListener;

import java.util.Map;
import java.util.Set;

public interface IListenerRepository {

    @NotNull
    Map<String, AbstractListener> getCommands();

    @NotNull
    Set<String> getCommandsName();

    @NotNull
    Set<String> getCommandsArg();

}
