package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    void saveCookieHeaders(@Nullable Object port);

    void setCookieHeadersForRequest(@Nullable Object port);

    boolean isAuth();

}
