package ru.malakhov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.malakhov.tm.endpoint.*;

import javax.xml.ws.BindingProvider;
import java.util.Map;

@Configuration
@ComponentScan("ru.malakhov.tm")
public class ApplicationConfig {

    @Bean
    @NotNull
    public AuthenticationEndpointService authenticationEndpointService() {
        return new AuthenticationEndpointService();
    }

    @Bean
    @NotNull
    public IAuthenticationEndpoint authenticationEndpoint(
            @NotNull final AuthenticationEndpointService authenticationEndpointService
    ) {
        @NotNull final IAuthenticationEndpoint authenticationEndpoint =
                authenticationEndpointService.getAuthenticationEndpointPort();
        setMaintain(authenticationEndpoint);
        return authenticationEndpoint;
    }

    @Bean
    @NotNull
    public AdminEndpointService adminUserEndpointService() {
        return new AdminEndpointService();
    }

    @Bean
    @NotNull
    public IAdminEndpoint adminUserEndpoint(
            @NotNull final AdminEndpointService adminEndpointService
    ) {
        @NotNull final IAdminEndpoint adminEndpoint =
                adminEndpointService.getAdminEndpointPort();
        setMaintain(adminEndpoint);
        return adminEndpoint;
    }

    @Bean
    @NotNull
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint(
            @NotNull final UserEndpointService userEndpointService
    ) {
        @NotNull final IUserEndpoint userEndpoint =
                userEndpointService.getUserEndpointPort();
        setMaintain(userEndpoint);
        return userEndpoint;
    }

    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint(
            @NotNull final ProjectEndpointService projectEndpointService
    ) {
        @NotNull final IProjectEndpoint projectEndpoint =
                projectEndpointService.getProjectEndpointPort();
        setMaintain(projectEndpoint);
        return projectEndpoint;
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint(
            @NotNull final TaskEndpointService taskEndpointService
    ) {
        @NotNull final ITaskEndpoint taskEndpoint =
                taskEndpointService.getTaskEndpointPort();
        setMaintain(taskEndpoint);
        return taskEndpoint;
    }

    private void setMaintain(@NotNull final Object port) {
        @NotNull final BindingProvider bindingProvider = (BindingProvider) port;
        @NotNull final Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

}
