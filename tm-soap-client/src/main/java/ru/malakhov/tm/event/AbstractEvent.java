package ru.malakhov.tm.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AbstractEvent {

    @NotNull
    protected String name;

    public AbstractEvent(@NotNull final String name) {
        this.name = name;
    }

}
