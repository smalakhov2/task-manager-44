package ru.malakhov.tm.event;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class RunEvent extends AbstractEvent {

    public RunEvent(@NotNull final String name) {
        super(name);
    }

}
