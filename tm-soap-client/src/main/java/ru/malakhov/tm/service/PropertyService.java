package ru.malakhov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.repository.IPropertyRepository;
import ru.malakhov.tm.api.service.IPropertyService;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@NoArgsConstructor
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String HEADER_COOKIE = "Cookie";

    @NotNull
    private final String HEADER_SET_COOKIE = "Set-Cookie";

    @NotNull
    @Autowired
    private IPropertyRepository propertyRepository;

    @Nullable
    private Map<String, Object> getRequestContext(@NotNull final Object port) {
        @Nullable final BindingProvider bindingProvider = getBindingProvider(port);
        return bindingProvider.getRequestContext();
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private Map<String, Object> getHttpRequestHeaders(@NotNull final Object port) {
        @Nullable final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    private void setListCookieRowRequest(
            @NotNull final Object port,
            @NotNull final List<String> value) {
        @Nullable final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext != null && getHttpRequestHeaders(port) == null) {
            requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<String, Object>());
        }
        final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(HEADER_COOKIE, value);
    }

    @NotNull
    private BindingProvider getBindingProvider(@NotNull final Object port) {
        return (BindingProvider) port;
    }

    @Nullable
    private Map<String, Object> getResponseContext(@NotNull final Object port) {
        @NotNull final BindingProvider bindingProvider = getBindingProvider(port);
        return bindingProvider.getResponseContext();
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private Map<String, Object> getHttpResponseHeaders(@NotNull final Object port) {
        @Nullable final Map<String, Object> responseContext = getResponseContext(port);
        if (responseContext == null) return null;
        return (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private List<String> getListSetCookieRow(@NotNull final Object port) {
        @Nullable final Map<String, Object> httpResponseHeaders = getHttpResponseHeaders(port);
        if (httpResponseHeaders == null) return null;
        return (List<String>) httpResponseHeaders.get(HEADER_SET_COOKIE);
    }

    @NotNull
    public List<String> getCookieHeaders() {
        return propertyRepository.getCookieHeaders();
    }

    public void setCookieHeaders(@Nullable final List<String> cookieHeaders) {
        if (cookieHeaders == null) return;
        propertyRepository.setCookieHeaders(cookieHeaders);
    }

    public void saveCookieHeaders(@Nullable final Object port) {
        if (port == null) return;
        @Nullable final List<String> cookieHeaders = getListSetCookieRow(port);
        if (cookieHeaders == null || cookieHeaders.isEmpty()) return;
        propertyRepository.setCookieHeaders(cookieHeaders);
    }

    public void setCookieHeadersForRequest(@Nullable final Object port) {
        if (port == null) return;
        @NotNull final List<String> cookieHeaders = getCookieHeaders();
        if (cookieHeaders.isEmpty()) return;
        setListCookieRowRequest(port, cookieHeaders);
    }

    @Override
    public boolean isAuth() {
        return getCookieHeaders().isEmpty();
    }

}