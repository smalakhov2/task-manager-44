package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.ITaskEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class TaskUpdateByIdListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = consoleProvider.nextLine();
        System.out.print("ENTER NEW NAME: ");
        @NotNull final String newName = consoleProvider.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        @NotNull final String newDescription = consoleProvider.nextLine();
        propertyService.setCookieHeadersForRequest(taskEndpoint);
        @NotNull final Result result = taskEndpoint.updateTaskById(
                id, newName, newDescription
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
