package ru.malakhov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.AccessDeniedException_Exception;
import ru.malakhov.tm.endpoint.IUserEndpoint;
import ru.malakhov.tm.endpoint.UserDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class UserProfileShowListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "show-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    @EventListener(condition = "@userProfileShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception, AccessDeniedException_Exception {
        System.out.println("[YOUR-PROFILE]");
        propertyService.setCookieHeadersForRequest(userEndpoint);
        @Nullable final UserDto user = userEndpoint.profile();
        if (user == null) {
            System.out.println("[Error]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

    @Override
    public boolean secure() {
        return true;
    }

}
