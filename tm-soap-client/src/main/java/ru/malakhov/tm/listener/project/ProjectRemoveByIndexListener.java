package ru.malakhov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.IProjectEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class ProjectRemoveByIndexListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by index.";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = consoleProvider.nextNumber() - 1;
        propertyService.setCookieHeadersForRequest(projectEndpoint);
        @NotNull final Result result = projectEndpoint.removeProjectByIndex(index);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
