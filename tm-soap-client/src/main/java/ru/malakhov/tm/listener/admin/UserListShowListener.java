package ru.malakhov.tm.listener.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.IAdminEndpoint;
import ru.malakhov.tm.endpoint.UserDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

import java.util.List;

@Component
public class UserListShowListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IAdminEndpoint adminEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user list.";
    }

    @Override
    @EventListener(condition = "@userListShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[USER-LIST]");
        propertyService.setCookieHeadersForRequest(adminEndpoint);
        @NotNull final List<UserDto> users = adminEndpoint.getAllUserList();
        int index = 1;
        for (@Nullable final UserDto user : users) {
            if (user != null) {
                System.out.println(index + ". " + user.getLogin());
                index++;
            }
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
