package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IListenerService;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.event.RunEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.ListenerService;

import java.util.Collection;

@Component
public class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    private void execute() {
        System.out.println("[HELP]");
        @NotNull final IListenerService listenerService = context.getBean(ListenerService.class);
        @NotNull final Collection<AbstractListener> listeners = listenerService.getCommands().values();
        for (@NotNull final AbstractListener listener : listeners)
            System.out.println(listener);
    }

    @Override
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        execute();
    }

    @EventListener(condition = "@helpListener.arg() == #event.name")
    public void handler(@NotNull final RunEvent event) {
        execute();
    }

    @Override
    public boolean secure() {
        return false;
    }

}
