package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.ITaskEndpoint;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class TaskRemoveByIndexListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = consoleProvider.nextNumber() - 1;
        propertyService.setCookieHeadersForRequest(taskEndpoint);
        @NotNull final Result result = taskEndpoint.removeTaskByIndex(index);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
