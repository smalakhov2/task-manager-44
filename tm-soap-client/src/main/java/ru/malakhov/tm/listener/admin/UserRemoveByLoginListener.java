package ru.malakhov.tm.listener.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.*;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

import java.util.Collections;

@Component
public class UserRemoveByLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IAdminEndpoint adminEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-remove";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    @EventListener(condition = "@userRemoveByLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception, AccessDeniedException_Exception {
        System.out.println("[USER-REMOVE]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = consoleProvider.nextLine();
        propertyService.setCookieHeadersForRequest(adminEndpoint);
        propertyService.setCookieHeadersForRequest(userEndpoint);
        @Nullable final UserDto currentUser = userEndpoint.profile();
        if (currentUser == null) {
            System.out.println("[Error]");
            return;
        }
        if (login.equals(currentUser.getLogin())) {
            String answer = "";
            do {
                System.out.print("THIS IS YOUR ACCOUNT. DELETE IT(Y/N): ");
                answer = consoleProvider.nextLine().toLowerCase();
            } while (!answer.equals("y") && !answer.equals("n"));
            if (answer.equals("n")) {
                System.out.println("[CANCEL]");
                return;
            }
            propertyService.setCookieHeaders(Collections.emptyList());
        }
        @NotNull final Result result = adminEndpoint.removeUserByLogin(login);
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}
