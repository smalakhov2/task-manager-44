package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.ITaskEndpoint;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.service.PropertyService;

@Component
public class TaskShowByIndexListener extends AbstractTaskShowListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    @EventListener(condition = "@taskShowByIndexListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = consoleProvider.nextNumber() - 1;
        propertyService.setCookieHeadersForRequest(taskEndpoint);
        @Nullable final TaskDto task = taskEndpoint.getTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        showTask(task);
        System.out.println("[OK]");
    }


    @Override
    public boolean secure() {
        return true;
    }

}
