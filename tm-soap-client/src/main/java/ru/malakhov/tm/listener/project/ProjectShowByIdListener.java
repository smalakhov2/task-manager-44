package ru.malakhov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.IProjectEndpoint;
import ru.malakhov.tm.endpoint.ProjectDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.service.PropertyService;

@Component
public class ProjectShowByIdListener extends AbstractProjectShowListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = consoleProvider.nextLine();
        propertyService.setCookieHeadersForRequest(projectEndpoint);
        @Nullable final ProjectDto project =
                projectEndpoint.getProjectById(id);
        if (project == null) System.out.println("[FAIL]");
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
