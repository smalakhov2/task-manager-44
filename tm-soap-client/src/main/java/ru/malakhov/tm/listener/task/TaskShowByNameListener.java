package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.ITaskEndpoint;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.service.PropertyService;

import java.util.List;

@Component
public class TaskShowByNameListener extends AbstractTaskShowListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    @EventListener(condition = "@taskShowByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER NAME: ");
        @NotNull final String name = consoleProvider.nextLine();
        propertyService.setCookieHeadersForRequest(taskEndpoint);
        @NotNull final List<TaskDto> tasks = taskEndpoint.getTasksByName(name);
        for (@NotNull final TaskDto task : tasks) showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}
