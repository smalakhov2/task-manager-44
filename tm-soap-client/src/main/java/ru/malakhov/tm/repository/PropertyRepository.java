package ru.malakhov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.api.repository.IPropertyRepository;

import java.util.Collections;
import java.util.List;

@Repository
@NoArgsConstructor
public final class PropertyRepository implements IPropertyRepository {

    @NotNull
    private List<String> cookieHeaders = Collections.emptyList();

    @NotNull
    @Override
    public List<String> getCookieHeaders() {
        return cookieHeaders;
    }

    @Override
    public void setCookieHeaders(@NotNull final List<String> cookieHeaders) {
        this.cookieHeaders = cookieHeaders;
    }

}
