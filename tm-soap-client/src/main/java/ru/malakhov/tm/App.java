package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.*;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class App {

    public static final String SLASH = "/";

    public static final String HEADER_COOKIE = "Cookie";

    public static final String HEADER_SET_COOKIE = "Set-Cookie";

    public static void main(String[] args) throws AbstractException_Exception {
        AuthenticationEndpointService authenticationEndpointService = new AuthenticationEndpointService();
        IAuthenticationEndpoint authenticationEndpoint = authenticationEndpointService.getAuthenticationEndpointPort();
        setMaintain(authenticationEndpoint);

        System.out.println(authenticationEndpoint.login("test", "test"));
        final List<String> session = getListSetCookieRow(authenticationEndpoint);


        System.out.println(getListSetCookie(authenticationEndpoint));

        ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        IProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
        setMaintain(projectEndpoint);
        setListCookieRowRequest(projectEndpoint, session);
        System.out.println(projectEndpoint.getProjectList());
    }

    public static void setMaintain(@NotNull final Object port) {
        @NotNull final BindingProvider bindingProvider = (BindingProvider) port;
        @NotNull final Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

    public static BindingProvider getBindingProvider(@Nullable final Object port) {
        if (port == null) return null;
        return (BindingProvider) port;
    }

    public static Map<String, Object> getResponseContext(@Nullable final Object port) {
        @Nullable final BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getResponseContext();
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getHttpResponseHeaders(@Nullable final Object port) {
        @Nullable final Map<String, Object> responseContext = getResponseContext(port);
        if (responseContext == null) return null;
        return (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
    }

    @SuppressWarnings("unchecked")
    public static List<String> getListSetCookieRow(@Nullable final Object port) {
        @Nullable final Map<String, Object> httpResponseHeaders = getHttpResponseHeaders(port);
        if (httpResponseHeaders == null) return null;
        return (List<String>) httpResponseHeaders.get(HEADER_SET_COOKIE);
    }

    public static List<HttpCookie> getListSetCookie(@Nullable final Object port) {
        @Nullable final List<String> listSetCookieRow = getListSetCookieRow(port);
        if (listSetCookieRow == null) return null;
        @NotNull final List<HttpCookie> result = new ArrayList<>();
        for (@NotNull final String cookieRow : listSetCookieRow) {
            @NotNull final List<HttpCookie> httpCookies = HttpCookie.parse(cookieRow);
            @NotNull final HttpCookie httpCookie = httpCookies.get(0);
            result.add(httpCookie);
        }
        return result;
    }

    public static Map<String, Object> getRequestContext(@Nullable final Object port) {
        @Nullable final BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getRequestContext();
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> getHttpRequestHeaders(@Nullable final Object port) {
        @Nullable final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    public static void setListCookieRowRequest(
            @Nullable final Object port,
            @Nullable final List<String> value) {
        if (getRequestContext(port) != null && getHttpRequestHeaders(port) == null) {
            getRequestContext(port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<String, Object>());
        }
        final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(HEADER_COOKIE, value);
    }

}
