package ru.malakhov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.exception.AbstractException;

public final class NumberIncorrectException extends AbstractException {

    public NumberIncorrectException(@NotNull final String value, @NotNull final Throwable cause) {
        super("Error! This value ``" + value + "`` is not number", cause);
    }

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

}
