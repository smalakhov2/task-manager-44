package ru.malakhov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-06T21:18:32.154+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.api.tm.malakhov.ru/", name = "IAuthenticationEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IAuthenticationEndpoint {

    @WebMethod
    @RequestWrapper(localName = "logout", targetNamespace = "http://endpoint.api.tm.malakhov.ru/", className = "Logout")
    @ResponseWrapper(localName = "logoutResponse", targetNamespace = "http://endpoint.api.tm.malakhov.ru/", className = "LogoutResponse")
    @WebResult(name = "return", targetNamespace = "")
    public Result logout();

    @WebMethod
    @RequestWrapper(localName = "login", targetNamespace = "http://endpoint.api.tm.malakhov.ru/", className = "Login")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://endpoint.api.tm.malakhov.ru/", className = "LoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public Result login(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );
}
