package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.repository.entity.IUserRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
@Transactional
public class TaskServiceTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user1 = new User();
    final @NotNull User user2 = new User();

    @Before
    public void initData() {
        userRepository.save(user1);
        userRepository.save(user2);
    }

    @After
    public void clearData() {
        taskService.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void createTest() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create(user1.getId(), "Test");
        Assert.assertEquals("Test", taskService.findAll().get(0).getName());
    }

    @Test
    public void updateDTOTest() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.create(user1.getId(), "Test");
        Assert.assertEquals("Test", taskService.findAll().get(0).getName());
        final @NotNull TaskDTO taskDTO = taskService.findAllDTOByUserId(user1.getId()).get(0);
        taskDTO.setName("taskDTO");
        taskService.updateDTO(user1.getId(), taskDTO);
        Assert.assertEquals("taskDTO", taskService.findAllDTOByUserId(user1.getId()).get(0).getName());
    }

    @Test
    public void findAllTest() {
        taskService.create(user1.getId(), "Test");
        taskService.create(user1.getId(), "Test2");
        Assert.assertEquals(2, taskService.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        taskService.create(user1.getId(), "Test");
        taskService.create(user1.getId(), "Test2");
        taskService.create(user2.getId(), "Test3");
        Assert.assertEquals(2, taskService.findAllByUserId(user1.getId()).size());
        Assert.assertEquals(1, taskService.findAllByUserId(user2.getId()).size());
    }

    @Test
    public void findAllDTOByUserIdTest() {
        taskService.create(user1.getId(), "Test");
        taskService.create(user1.getId(), "Test2");
        taskService.create(user2.getId(), "Test3");
        Assert.assertEquals(2, taskService.findAllDTOByUserId(user1.getId()).size());
        Assert.assertEquals(1, taskService.findAllDTOByUserId(user2.getId()).size());
    }

    @Test
    public void deleteAllTest() {
        taskService.create(user1.getId(), "Test");
        taskService.create(user1.getId(), "Test2");
        Assert.assertEquals(2, taskService.findAllDTOByUserId(user1.getId()).size());
        taskService.deleteAll();
        Assert.assertEquals(0, taskService.findAllDTOByUserId(user1.getId()).size());
    }

    @Test
    public void deleteAllByUserIdTest() {
        taskService.create(user1.getId(), "Test");
        taskService.create(user1.getId(), "Test2");
        taskService.create(user2.getId(), "Test3");
        Assert.assertEquals(2, taskService.findAllDTOByUserId(user1.getId()).size());
        Assert.assertEquals(1, taskService.findAllDTOByUserId(user2.getId()).size());
        taskService.deleteAllByUserId(user1.getId());
        Assert.assertEquals(0, taskService.findAllDTOByUserId(user1.getId()).size());
        taskService.deleteAllByUserId(user2.getId());
        Assert.assertEquals(0, taskService.findAllDTOByUserId(user2.getId()).size());

    }

    @Test
    public void findOneByIdDTOTest() {
        taskService.create(user1.getId(), "Test");
        final @NotNull TaskDTO taskDTO = taskService.findAllDTOByUserId(user1.getId()).get(0);
        final @NotNull String id = taskDTO.getId();
        taskDTO.setName("taskDTO");
        taskService.updateDTO(user1.getId(), taskDTO);
        Assert.assertEquals(taskService.findOneByIdDTO(user1.getId(),id).getName(), taskDTO.getName());
    }

    @Test
    public void removeOneByIdTest() {
        taskService.create(user1.getId(), "Test");
        final @NotNull TaskDTO taskDTO = taskService.findAllDTOByUserId(user1.getId()).get(0);
        final @NotNull String id = taskDTO.getId();
        Assert.assertFalse(taskService.findAll().isEmpty());
        taskService.removeOneById(user1.getId(), id);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void existsByUserIdAndIdTest() {
        taskService.create(user1.getId(), "Test");
        final @NotNull TaskDTO taskDTO = taskService.findAllDTOByUserId(user1.getId()).get(0);
        final @NotNull String id = taskDTO.getId();
        Assert.assertTrue(taskService.existsByUserIdAndId(user1.getId(), id));
        taskService.removeOneById(user1.getId(), id);
        Assert.assertFalse(taskService.existsByUserIdAndId(user1.getId(), id));
    }

}
