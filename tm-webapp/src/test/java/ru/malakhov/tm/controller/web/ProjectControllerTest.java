package ru.malakhov.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.controller.AbstractControllerTest;
import ru.malakhov.tm.dto.ProjectDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ProjectControllerTest extends AbstractControllerTest {

    @Autowired
    private IProjectService projectService;

    @After
    public void clearProjects() {
        projectService.deleteAll();
    }

    @Test
    public void projectsIndexTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/projects"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project/project-list"));
    }

    @Test
    public void createTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/project/create"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void deleteTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/project/create"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void editGetTest() throws Exception {
        final @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("project");
        projectDTO.setUserId(user.getId());
        projectService.updateDTO(user.getId(), projectDTO);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/project/edit/" + projectDTO.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("project/project-edit"));
    }

    @Test
    public void EditPostTest() throws Exception {
        final @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("project");
        projectDTO.setUserId(user.getId());
        projectService.updateDTO(user.getId(), projectDTO);

        projectDTO.setName("newName");
        mockMvc.perform(MockMvcRequestBuilders
                .post("/project/edit/" + projectDTO.getId())
                .flashAttr("project", projectDTO))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/projects"));

        final @NotNull ProjectDTO projectFromBase = projectService.findOneByIdDTO(user.getId(), projectDTO.getId());
        Assert.assertEquals("newName", projectFromBase.getName());
    }

}