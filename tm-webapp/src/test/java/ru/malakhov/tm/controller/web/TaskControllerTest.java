package ru.malakhov.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.malakhov.tm.controller.AbstractControllerTest;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.dto.TaskDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class TaskControllerTest extends AbstractControllerTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @After
    public void clearTasks() {
        taskService.deleteAll();
    }

    @Test
    public void tasksIndexTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/tasks"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task/task-list"));
    }

    @Test
    public void createTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/task/create"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void deleteTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/task/create"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));
    }

    @Test
    public void editGetTest() throws Exception {
        final @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("task");
        taskDTO.setUserId(user.getId());
        taskService.updateDTO(user.getId(), taskDTO);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/task/edit/" + taskDTO.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task/task-edit"));
    }

    @Test
    public void editPostTest() throws Exception {
        final @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("task");
        taskDTO.setUserId(user.getId());
        taskService.updateDTO(user.getId(), taskDTO);
        taskDTO.setName("newName");

        final @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectService.updateDTO(user.getId(), projectDTO);
        taskDTO.setProjectId(projectDTO.getId());
        mockMvc.perform(MockMvcRequestBuilders
                .post("/task/edit/" + taskDTO.getId())
                .flashAttr("task", taskDTO))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks"));

        final @NotNull TaskDTO taskFromBase = taskService.findOneByIdDTO(user.getId(), taskDTO.getId());
        Assert.assertEquals("newName", taskFromBase.getName());
    }

}