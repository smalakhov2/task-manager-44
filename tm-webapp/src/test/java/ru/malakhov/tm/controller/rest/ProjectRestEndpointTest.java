package ru.malakhov.tm.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.malakhov.tm.controller.AbstractControllerTest;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDTO;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class ProjectRestEndpointTest extends AbstractControllerTest {

    @Autowired
    private IProjectService projectService;

    @After
    public void clearProjects() {
        projectService.deleteAll();
    }

    @Test
    public void createTest() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString("project");

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/project/create")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        final ProjectDTO projectFromBase = projectService.findAllDTOByUserId(user.getId()).get(0);
        Assert.assertEquals("\"project\"", projectFromBase.getName());
    }

    @Test
    public void updateTest() throws Exception {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("project");
        projectDTO.setDescription("project");
        projectDTO.setUserId(user.getId());
        projectService.updateDTO(user.getId(), projectDTO);
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(projectDTO);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .put("/api/project")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        final ProjectDTO projectFromBase = projectService.findOneByIdDTO(user.getId(), projectDTO.getId());
        Assert.assertEquals(projectDTO.getName(), projectFromBase.getName());
    }

    @Test
    public void findOneByIdDTODTO() throws Exception {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("projectName");
        projectDTO.setDescription("projectDescription");
        projectDTO.setUserId(user.getId());
        projectService.updateDTO(user.getId(), projectDTO);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/project/${id}", projectDTO.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name").value(projectDTO.getName()))
                .andExpect(jsonPath("$.description").value(projectDTO.getDescription()));

    }

    @Test
    public void existsByIdTest() throws Exception {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("projectName");
        projectDTO.setDescription("projectDescription");
        projectDTO.setUserId(user.getId());
        projectService.updateDTO(user.getId(), projectDTO);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/project/exist/${id}", projectDTO.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("true"));

        result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/project/exist/${id}", "dfsdfsd")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("false"));
    }

    @Test
    public void deleteOneByIdTest() throws Exception {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("projectName");
        projectDTO.setDescription("projectDescription");
        projectDTO.setUserId(user.getId());
        projectService.updateDTO(user.getId(), projectDTO);
        Assert.assertNotNull(projectService.findOneByIdDTO(user.getId(), projectDTO.getId()));

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/project/${id}", projectDTO.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void getListDTOTest() throws Exception {
        projectService.create(user.getId(), "project1");
        projectService.create(user.getId(), "project2");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/projects")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem("project1")))
                .andExpect(jsonPath("$..name", hasItem("project2")));
    }

    @Test
    public void deleteAllTest() throws Exception {
        projectService.create(user.getId(), "project1");
        projectService.create(user.getId(), "project2");
        projectService.create(user.getId(), "project3");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/projects/all")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(0, projectService.findAll().size());
    }

}