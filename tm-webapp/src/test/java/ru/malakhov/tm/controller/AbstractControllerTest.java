package ru.malakhov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public abstract class AbstractControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    protected MockMvc mockMvc;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    protected IUserService userService;

    protected User user;

    @Before
    public void initData() {
        userService.create("test", "test");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        user = userService.findAll().get(0);
        final @NotNull UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final @NotNull Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void clearData() {
        userService.deleteAll();
    }

}