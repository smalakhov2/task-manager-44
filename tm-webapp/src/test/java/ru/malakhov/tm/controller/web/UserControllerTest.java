package ru.malakhov.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.malakhov.tm.controller.AbstractControllerTest;
import ru.malakhov.tm.dto.UserDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UserControllerTest extends AbstractControllerTest {

    @Test
    public void registrationGetTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/registration"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("user/user-registration"));
    }

    @Test
    public void registrationPostTest() throws Exception {
        final @NotNull UserDTO userDTO = new UserDTO("userDTO", "pass");

        mockMvc.perform(MockMvcRequestBuilders
                .post("/registration")
                .flashAttr("newUser", userDTO))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));

        Assert.assertEquals(2, userService.findAll().size());
    }

}