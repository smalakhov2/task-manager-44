package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.repository.entity.IUserRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectRepositoryDTOTest {

    @Autowired
    private IProjectRepositoryDTO projectRepositoryDTO;

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user1 = new User();
    final @NotNull User user2 = new User();

    @Before
    @Transactional
    public void initData() {
        userRepository.save(user1);
        userRepository.save(user2);
    }

    @After
    @Transactional
    public void clearData() {
        userRepository.deleteAll();
    }

    @Test
    @Transactional
    public void findAllByUserIdTest() {
        final @NotNull ProjectDTO projectDTO1 = new ProjectDTO();
        final @NotNull ProjectDTO projectDTO2 = new ProjectDTO();
        final @NotNull ProjectDTO projectDTO3 = new ProjectDTO();

        projectDTO1.setUserId(user1.getId());
        projectDTO2.setUserId(user1.getId());
        projectDTO3.setUserId(user2.getId());

        projectRepositoryDTO.save(projectDTO1);
        projectRepositoryDTO.save(projectDTO2);
        projectRepositoryDTO.save(projectDTO3);

        Assert.assertEquals(2, projectRepositoryDTO.findAllByUserId(user1.getId()).size());
        Assert.assertEquals(1, projectRepositoryDTO.findAllByUserId(user2.getId()).size());

        projectRepositoryDTO.delete(projectDTO1);
        projectRepositoryDTO.delete(projectDTO2);
        projectRepositoryDTO.delete(projectDTO3);
    }

    @Test
    public void findOneByUserIdAndIdTest() {
        final @NotNull ProjectDTO projectDTO1 = new ProjectDTO();
        final @NotNull ProjectDTO projectDTO2 = new ProjectDTO();

        projectDTO1.setUserId(user1.getId());
        projectDTO2.setUserId(user2.getId());

        projectRepositoryDTO.save(projectDTO1);
        projectRepositoryDTO.save(projectDTO2);

        Assert.assertEquals(projectDTO1.getId(), projectRepositoryDTO.findOneByUserIdAndId(user1.getId(), projectDTO1.getId()).getId());
        Assert.assertEquals(projectDTO2.getId(), projectRepositoryDTO.findOneByUserIdAndId(user2.getId(), projectDTO2.getId()).getId());

        projectRepositoryDTO.delete(projectDTO1);
        projectRepositoryDTO.delete(projectDTO2);
    }

    @Test
    public void existsByIdAndUserIdTest() {
        final @NotNull ProjectDTO projectDTO1 = new ProjectDTO();
        final @NotNull ProjectDTO projectDTO2 = new ProjectDTO();

        projectDTO1.setUserId(user1.getId());
        projectDTO2.setUserId(user2.getId());

        projectRepositoryDTO.save(projectDTO1);
        projectRepositoryDTO.save(projectDTO2);

        Assert.assertTrue(projectRepositoryDTO.existsByUserIdAndId(user1.getId(), projectDTO1.getId()));
        Assert.assertTrue(projectRepositoryDTO.existsByUserIdAndId(user2.getId(), projectDTO2.getId()));

        projectRepositoryDTO.delete(projectDTO1);
        projectRepositoryDTO.delete(projectDTO2);
    }

}