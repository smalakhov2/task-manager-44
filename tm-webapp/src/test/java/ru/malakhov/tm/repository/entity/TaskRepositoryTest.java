package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TaskRepositoryTest {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user1 = new User();
    final @NotNull User user2 = new User();

    @Before
    @Transactional
    public void initData() {
        userRepository.save(user1);
        userRepository.save(user2);
    }

    @After
    @Transactional
    public void clearData() {
        userRepository.deleteAll();
        taskRepository.deleteAll();
    }

    @Test
    @Transactional
    public void findAllByUserIdTest() {
        final @NotNull Task task1 = new Task();
        final @NotNull Task task2 = new Task();
        final @NotNull Task task3 = new Task();

        task1.setUser(user1);
        task2.setUser(user1);
        task3.setUser(user2);

        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);

        Assert.assertEquals(2, taskRepository.findAllByUserId(user1.getId()).size());
        Assert.assertEquals(1, taskRepository.findAllByUserId(user2.getId()).size());

        taskRepository.delete(task1);
        taskRepository.delete(task2);
        taskRepository.delete(task3);
    }

    @Test
    @Transactional
    public void deleteAllByUserIdTest() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());

        final @NotNull Task task1 = new Task();
        task1.setUser(user1);
        taskRepository.save(task1);

        final @NotNull Task task2 = new Task();
        task2.setUser(user1);
        taskRepository.save(task2);

        Assert.assertFalse(taskRepository.findAll().isEmpty());

        taskRepository.deleteAllByUserId(user1.getId());

        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    @Transactional
    public void deleteOneByUserIdAndIdTest() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());

        final @NotNull Task task1 = new Task();
        task1.setUser(user1);
        taskRepository.save(task1);

        Assert.assertTrue(taskRepository.existsById(task1.getId()));

        taskRepository.deleteOneByUserIdAndId(user1.getId(), task1.getId());

        Assert.assertFalse(taskRepository.existsById(task1.getId()));
    }

}