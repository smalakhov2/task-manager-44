package ru.malakhov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.AbstractDTO;


@Repository
public interface IRepositoryDTO<E extends AbstractDTO> extends JpaRepository<E, String> {

}
