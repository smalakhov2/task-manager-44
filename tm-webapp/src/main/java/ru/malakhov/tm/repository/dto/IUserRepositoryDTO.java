package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.UserDTO;

@Repository
public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    @Nullable
    UserDTO findOneById(final @NotNull String id);

    @Nullable
    UserDTO findOneByLogin(final @NotNull String login);

}
