package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.ProjectDTO;

import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectsRestEndpoint {

    @GetMapping
    List<ProjectDTO> getListDTO();

    @DeleteMapping("/all")
    void deleteAll();

}
