package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IService<E extends AbstractEntity> {

    void load(@Nullable Collection<E> e);

}
