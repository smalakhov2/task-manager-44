package ru.malakhov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @WebMethod
    public List<ProjectDTO> getListDTO() {
        return projectService.findAllDTOByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public void deleteAll() {
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public void create(@WebParam(name = "name") String name) {
        projectService.create(UserUtil.getUserId(), name);
    }

    @WebMethod
    public void update(@WebParam(name = "project") ProjectDTO projectDTO) {
        projectService.updateDTO(UserUtil.getUserId(), projectDTO);
    }

    @WebMethod
    public ProjectDTO findOneByIdDTO(@WebParam(name = "id") String id) {
        return projectService.findOneByIdDTO(UserUtil.getUserId(), id);
    }

    @WebMethod
    public boolean existsById(@WebParam(name = "id") String id) {
        return projectService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteOneById(@WebParam(name = "id") String id) {
        projectService.removeOneById(UserUtil.getUserId(), id);
    }

}
