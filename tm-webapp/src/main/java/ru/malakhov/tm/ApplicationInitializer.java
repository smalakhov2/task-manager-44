package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.malakhov.tm.config.ApplicationConfiguration;
import ru.malakhov.tm.config.WebApplicationConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebApplicationConfiguration.class };
    }

    @Override
    protected String @NotNull [] getServletMappings() {
        return new String[] { "/" };
    }

}
