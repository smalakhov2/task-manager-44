package ru.malakhov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED("NOT STARTED"),
    IN_PROGRESS("IN PROGRESS"),
    COMPLETED("COMPLETED");

    private final @NotNull String displayName;

    Status(final @NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
